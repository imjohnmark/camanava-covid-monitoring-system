@extends('layouts.app')

@section('content')
<div class="row mt-4 mb-2">
        <div class="col-sm-8 offset-sm-2">
            <h1>Update Barangay</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <form action="/barangays/{{ $barangays->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" value="{{ $barangays->name }}" class="form-control" id="name" placeholder="Enter name of brgy">
                </div>
                <div class="form-group">
                    <label for="city">City <span class="required">*</span></label>
                    <select class="form-control" id="city" name="city">
                        <option value="">--Select City--</option>
                        @foreach ($cities as $city)
                            @if ($barangays->city_id == $city->id)
                                <option value="{{ $city->id }}" selected="selected">{{ $city->name }}</option>
                            @else
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update</button> <a href="/barangays" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
@endsection
