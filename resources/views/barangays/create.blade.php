@extends('layouts.app')


@section('content')
<div class="row mt-5">
 <div class="col-sm-8 offset-sm-2">
    <h1>Add a Barangay</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('barangays.store') }}">
          @csrf
          <div class="form-group ">    
              <label for="first_name">Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>   
        <div class="form-group ">
                    <label for="city">City</label>
                    <select class="form-control" id="city" name="city">
                        <option value="">Select City</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select>
        </div>  
          <button type="submit" class="btn btn-warning">Add</button>
      </form>
  </div>
</div>
</div>
@endsection
