@extends('layouts.app')

@section('content')
<div class="row">
<div class="col-sm-8 offset-sm-2">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
<div class="row mt-4 mb-2">
<div class="col-sm-8 offset-sm-2">
    <div class="row">
        <div class="col-sm-6">
            <h1>Barangay</h1>
        </div>
        <div class="col-sm-6">
            <a href="/barangays/create" class="btn btn-warning pull-right float-right">Add Barangay</a>
        </div>
    </div>
    @if(count($barangays) > 0)
        <table class="table table-striped">
    <thead>
        <tr>
          <th>Name</th>
          <th>City</th>
          <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($barangays as $barangay)
        <tr>
            <td>{{$barangay->name}}</td>
            @foreach ($cities as $city)
                @if ($city->id == $barangay->city_id)
                    <td>{{$city->name}}</td>
                @endif
            @endforeach
            <td>
                    <a href="barangays/{{$barangay->id}}" class="btn btn-primary float-left mr-2">View</a>
                    <a href="/barangays/{{$barangay->id}}/edit" class="btn btn-success float-left mr-2"><i class="fa fa-edit"></i>Edit</a>
                    <form action="/barangays/{{ $barangay->id }}" method="POST" class="float-left">
                                        @csrf
                                        @method('DELETE')
                    <input class="btn btn-danger" type="submit" value="Delete" onclick="return confirm(`Are you sure you want to delete barangay '{{ $barangay->name }}'?`);">
                    </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
        {{$barangays->links()}}
    @else
        <p>No barangay found</p>
    @endif
    <div>
</div>
@endsection
