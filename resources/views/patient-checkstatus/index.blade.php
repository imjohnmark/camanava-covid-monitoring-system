@extends('layouts.app')

@section('content')
    <div class="row mt-4 mb-2">
        <div class="col-sm-8 offset-sm-2">
            <h1>Check your Status</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
        @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
            
              <form action="/patient-checkstatus" method="GET">
              <div class="input-group mb-3">
              <input type="text" class="form-control number-input" placeholder="Enter patient mobile number" aria-describedby="basic-addon2" name="search">
              <div class="input-group-append">
              <button class="btn btn-secondary" id="btn-search" type="submit">Submit</button>
              </div>
              </div>
              </form>
              
              <div class="patientCaseType">
              @foreach($patient as $patients)
              <h2 class="mt-5">Your Case Type: {{ $patients->case_type }}</h2>
              @endforeach
              </div>
      
    </div>
@endsection