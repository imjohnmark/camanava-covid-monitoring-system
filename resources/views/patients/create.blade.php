@extends('layouts.app')

@section('content')
    <div class="row mt-4 mb-2">
        <div class="col-sm-8 offset-sm-2">
            <h1>Add New Patient</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
        @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
             <form method="post" action="{{ route('patients.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name of patient">
                </div>
                <div class="form-group">
                    <label for="barangay">City <span class="required">*</span></label>
                    <select class="form-control" id="select-city" name="city">
                        <option value="">Select City</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="barangay">Barangay</label>
                     <select class="form-control" id="select-barangay" name="barangay">
                        <option value="">Select Barangay</option>
                        @foreach ($barangays as $barangay)
                            <option city-data-id="{{$barangay->city_id}}" value="{{ $barangay->id }}">{{ $barangay->name }}</option>
                        @endforeach
                    </select> 
                </div>
                <div class="form-group">
                    <label for="number">Contact No. <span class="required">*</span></label>
                    <input type="text" name="number" class="form-control" id="number" placeholder="Mobile / Tel. / Phone">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter your Email">
                </div>
                <div class="form-group">
                    <label for="case-type">Case Type <span class="required">*</span></label>
                    <select class="form-control" id="case-type" name="case_type">
                        <option value="">Select Case Type</option>
                        @foreach ($case_types as $case_type)
                            <option value="{{$case_type}}">{{$case_type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group hidden">
                    <label for="coronavirus-status">Corona Virus Status <span class="required">*</span></label>
                    <select class="form-control" id="coronavirus-status" name="coronavirus_status">
                        <option value="">Select Status</option>
                        @foreach ($status_list as $status)
                            <option value="{{$status}}">{{$status}}</option>
                        @endforeach
                    </select>
                </div>
                <input class="btn btn-primary" type="submit" value="Add"> <a href="/patients" class="btn btn-light">Cancel</a>
            </form>       
        </div>
    </div>
@endsection