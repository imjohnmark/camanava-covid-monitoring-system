@extends('layouts.app')

@section('content')
    <div class="row mt-4 mb-2">
        <div class="col-sm-6 offset-sm-2">
            <h1>Patient Information</h1>
        </div>
        <div class="col-sm-2">
            <a href="/patients" class="btn btn-success pull-right float-right">Go back</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <table class="table table-striped">
                <tr>
                    <th>Name</th>
                    <th>Barangay</th>
                    <th>City</th>
                    <th>Number</th>
                    <th>Email</th>
                    <th>Case Type</th>
                    <th>Corona Virus Status</th>
                </tr>
                <tr>
                    <td>{{ $patient->name }}</td>
                    <td>{{ $barangay->name }}</td>
                    <td>{{ $city->name }}</td>
                    <td>{{ $patient->number }}</td>
                    <td>{{ $patient->email !== null ? $patient->email : "N/A" }}</td>
                    <td>{{ $patient->case_type }}</td>
                    @if ($patient->case_type === "Positive")
                        <td>{{ $patient->coronavirus_status }}</td>
                    @endif
            </table>
        </div>
    </div>
@endsection