@extends('layouts.app')

@section('content')
<div class="row mt-4 mb-2">
    
<div class="col-sm-8 offset-sm-2">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
<div class="row">
    
<div class="col-sm-8 offset-sm-2">
    <div class="row">
        <div class="col-sm-6">
            <h1>Patient</h1>
        </div>
        <div class="col-sm-6">
            <a href="/patients/create" class="btn btn-warning pull-right float-right">Add Patient</a>
        </div>
    </div>
    @if(count($patients) > 0)
        <table class="table table-striped">
    <thead>
        <tr>
          <th>Name</th>
          <th>City</th>
          <th>Brgy</th>
          <th>Case Type</th>
          <th>Actions</th>
        </tr>
    </thead>
    <tbody>
      @foreach ($patients as $patient)
                            <tr>
                                <td>{{$patient->name}}</td>
                                @foreach ($barangays as $barangay)
                                    @if ($barangay->id == $patient->barangay_id)
                                        @foreach ($cities as $city)
                                            @if ($barangay->city_id == $city->id)
                                                <td>{{$city->name}}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                                @foreach ($barangays as $barangay)
                                    @if ($barangay->id == $patient->barangay_id)
                                        <td>{{$barangay->name}}</td>
                                    @endif
                                @endforeach
                                <td>{{$patient->case_type}}</td>
                                <td>
                                    <a href="/patients/{{$patient->id}}" class="btn btn-primary float-left mr-2">View</a>
                                    <a href="/patients/{{$patient->id}}/edit" class="btn btn-success float-left mr-2">Edit</a>
                                    <form action="{{ route('patients.destroy', $patient->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit" onclick="return confirm(`Are you sure you want to delete city '{{ $patient->name }}'?`);">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
    </tbody>
  </table>
        {{$patients->links()}}
    @else
        <p>No patient found</p>
    @endif
    <div>
</div>
@endsection
