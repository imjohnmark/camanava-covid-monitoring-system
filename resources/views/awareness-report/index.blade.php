@extends('layouts.app')

@section('content')
    <div class="row mt-4 mb-2">
        <div class="col-sm-8 offset-sm-2">
            <h1>Awareness Report</h1>
            <p>Select City and Barangay</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <form action="" method="GET">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" name="city" id="select-city">
                                <option value="">Select City</option>
                                @foreach ($cities as $city)
                                    @if (!empty($city_id))
                                        @if ($city_id == $city->id)
                                            <option value="{{$city->id}}" selected="selected">{{$city->name}}</option>
                                        @else
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endif
                                    @else
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" name="barangay" id="select-barangay">
                                <option value="">Select barangay</option>
                                @foreach ($barangays as $barangay)
                                    @if (!empty($barangay_id))
                                        @if ($barangay_id == $barangay->id)
                                            <option city-data-id="{{$barangay->city_id}}" value="{{$barangay->id}}" selected="selected">{{$barangay->name}}</option>
                                        @else
                                            <option city-data-id="{{$barangay->city_id}}" value="{{$barangay->id}}">{{$barangay->name}}</option>
                                        @endif
                                    @else
                                        <option city-data-id="{{$barangay->city_id}}" value="{{$barangay->id}}">{{$barangay->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <input class="btn btn-success" type="submit" value="Generate Report" id="generate-report">
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!empty($city_id))
        @if (!empty($barangay_id))
            <div class="row mt-3">
                <div class="col-sm-8 offset-sm-2">
                    <h4>{{$city_request->name}} - {{$barangay_request->name}} Case Reports</h4>
                    <table class="table table-striped">
                            <tr>
                                <th>PUI</th>
                                <th>PUM</th>
                                <th>Positive on Corona Virus</th>
                                <th>Negative on Corona Virus</th>
                                
                            </tr>
                            <tr>
                                <td>{{count($barangay_results_pui)}}</td>
                                <td>{{count($barangay_results_pum)}}</td>
                                <td>{{count($barangay_results_positive)}}</td>
                                <td>{{count($barangay_results_negative)}}</td>
                            </tr>
                    </table>
                </div>
            </div>
        @else
            <div class="row" >
                <div class="col">
                    <h4>{{$city_request->name}} Case Reports</h4>
                    <table class="table table-striped">
                            <tr>
                                <th>PUI</th>
                                <th>PUM</th>
                                <th>Positive on Corona Virus</th>
                                <th>Negative on Corona Virus</th>
                            </tr>
                            <tr>
                                <td>{{count($barangay_results_pui)}}</td>
                                <td>{{count($barangay_results_pum)}}</td>
                                <td>{{count($barangay_results_positive)}}</td>
                                <td>{{count($barangay_results_negative)}}</td>
                            </tr>
                    </table>
                </div>
            </div>
        @endif
    @endif
@endsection