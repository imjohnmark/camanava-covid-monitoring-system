@extends('layouts.app')

@section('content')
<div class="row mt-4 mb-2">
 <div class="col-sm-8 offset-sm-2">
    <h1>Add a City</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('cities.store') }}">
          @csrf
          <div class="form-group">    
              <label for="first_name">Name:</label>
              <input type="text" class="form-control" name="name" placeholder="Enter name of city"/>
          </div>                       
          <button type="submit" class="btn btn-success">Add</button>
      </form>
  </div>
</div>
</div>
@endsection
