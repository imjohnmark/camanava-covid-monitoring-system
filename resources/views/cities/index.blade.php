@extends('layouts.app')


@section('content')
<div class="row">
<div class="col-sm-8 offset-sm-2">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
<div class="row mt-4 mb-2">
<div class="col-sm-8 offset-sm-2">
    <div class="row">
        <div class="col-sm-6">
            <h1>City</h1>
        </div>
        <div class="col-sm-6">
            <a href="/cities/create" class="btn btn-warning pull-right float-right">Add City</a>
        </div>
    </div>
    @if(count($cities) > 0)
        <table class="table table-striped">
    <thead>
        <tr>
          <th>Name</th>
          <th colspan = 2>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cities as $city)
        <tr>
            <td>{{$city->name}}</td>
            <td>
                    <a href="cities/{{$city->id}}" class="btn btn-primary float-left mr-2">View</a>
                    <a href="/cities/{{$city->id}}/edit" class="btn btn-success float-left mr-2"><i class="fa fa-edit"></i>Edit</a>
                    <form action="/cities/{{$city->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit" onclick="return confirm(`Are you sure you want to delete city '{{ $city->name }}'?`);">Delete</button>
                    </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
        {{$cities->links()}}
    @else
        <p>No city found</p>
    @endif
    <div>
</div>
@endsection
