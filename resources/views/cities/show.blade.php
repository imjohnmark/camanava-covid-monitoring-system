@extends('layouts.app')

@section('content')
    <div class="row mt-4 mb-2">
        <div class="col-sm-6 offset-sm-2">
            <h1 >City Information</h1>
        </div>
        <div class="col-sm-2">
            <a href="/cities" class="btn btn-success pull-right float-right">Go back</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <table class="table table-striped">
                <tr>
                    <th>City</th>
                    
                </tr>
                <tr>
                    <td>{{ $city->name }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection