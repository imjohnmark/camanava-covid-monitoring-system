@extends('layouts.app')

@section('content')
<div class="row mt-4 mb-2">
    <div class="col-sm-8 offset-sm-2">
        <h1>Update City</h1>
    </div>
    </div>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <form action="/cities/{{ $city->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" value="{{ $city->name }}" class="form-control" id="name" placeholder="Enter name of city">
                </div>
                <button type="submit" class="btn btn-primary">Update</button> <a href="/cities" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
@endsection
