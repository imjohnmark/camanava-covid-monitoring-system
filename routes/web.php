<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('cities', 'CityController');
Route::resource('barangays', 'BarangayController');
Route::resource('patients', 'PatientController');
Route::get('/patient-checkstatus', 'CheckStatusController@index');
Route::get('/awareness-report', 'AwarenessReportsController@index');
Route::get('/coronavirus-report', 'CoronaVirusReportsController@index');


