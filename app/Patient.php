<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    // The attributes that are mass assignable
    protected $fillable = [
        'name',
        'barangay_id',
        'name',
        'number',
        'email',
        'case_type',
        'coronavirus_status'
    ];
    
    // Relationship barangay to patients
    public function barangay(){
        return $this->belongsTo(Barangay::class);
    }

}
