<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barangay;
use App\City;

class BarangayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /** Authentication for pages **/
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    } 

    public function index()
    {
        /**  Retrieve all barangays **/
        $barangays = Barangay::orderBy('name', 'asc')->paginate(10);
        $cities = City::all();
        return view('barangays.index')->with(['barangays' => $barangays, 'cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**  Create new barangay **/
        $cities = City::all();
        return view('barangays.create')->with(['cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** Form Validation **/
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required'
        ]);

        /** Create Barangay **/
        $barangay = new Barangay;
        $barangay->name = $request->input('name');
        $barangay->city_id = $request->input('city');
        $barangay->save();

        return redirect('/barangays')->with('success', 'Barangay Saved!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /**  Show barangay information **/
        $barangays = Barangay::find($id);
        $city = Barangay::find($id)->city;
        return view('barangays.show')->with(['barangays' => $barangays, 'city' => $city]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**  Edit city **/
        $barangays = Barangay::find($id);
        $cities = City::all();
        return view('barangays.edit')->with(['barangays' => $barangays, 'cities' => $cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**  Form validation **/ 
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required'
        ]);

        /**  Update barangay information **/
        $barangay = Barangay::find($id);
        $barangay->name = $request->input('name');
        $barangay->city_id = $request->input('city');
        $barangay->save();
        return redirect('/barangays')->with('success', 'Barangay Upated!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**  Delete a barangay **/
        $barangay = Barangay::find($id);
        $barangay->delete();
        return redirect('/barangays')->with('success', 'Barangay deleted!');
    }
}
