<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\City;
use App\Barangay;


class AwarenessReportsController extends Controller
{
    /** Authentication for pages **/
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**  Awareness Reports Views **/
    public function index(Request $request) {

        $city_id = $request->input('city');
        $barangay_id = $request->input('barangay');

        $barangays = Barangay::all();
        $cities = City::all();
        
        $city_request = City::find($city_id);
        $barangay_request = Barangay::find($barangay_id);

        /**  city only result **/
        $city_results_pui = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'PUI']
                            ])->get();

        $city_results_pum = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'PUM']
                            ])->get();

        $city_results_positive = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $city_results_negative = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Negative']
                            ])->get();
        
        /**  city with barangay result **/                   
        $barangay_results_pui = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'PUI']
                            ])->get();

        $barangay_results_pum = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'PUM']
                            ])->get();

        $barangay_results_positive = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $barangay_results_negative = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'Negative']
                            ])->get();

        return view('awareness-report.index')->with([
            'city_request' => $city_request, 
            'barangay_request' => $barangay_request, 
            'cities' => $cities, 
            'barangays' => $barangays, 
            'city_id' => $city_id, 
            'barangay_id' => $barangay_id,
            'city_results_pui' => $city_results_pui,
            'city_results_pum' => $city_results_pum,
            'city_results_positive' => $city_results_positive,
            'city_results_negative' => $city_results_negative,
            'barangay_results_pui' => $barangay_results_pui,
            'barangay_results_pum' => $barangay_results_pum,
            'barangay_results_positive' => $barangay_results_positive,
            'barangay_results_negative' => $barangay_results_negative
        ]);
    }
}
