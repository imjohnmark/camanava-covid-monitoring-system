<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\Barangay;
use App\City;
use App\Mail\CaseTypeChangeMail;
use Illuminate\Support\Facades\Mail;


class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** Authentication for pages **/
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    
    public function index()
    {
        /**  Retrieve all patients **/
        $patients = Patient::orderBy('name', 'asc')->paginate(10);
        $barangays = Barangay::all();
        $cities = City::all();
        return view('patients.index')->with(['patients' => $patients, 'barangays' => $barangays, 'cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**  Create new patient **/
        $barangays = Barangay::all();
        $cities = City::all();
        $case_types = ["PUI", "PUM", "Positive", "Negative"];
        $status_list = ["Active", "Recovered", "Deceased"];
        return view('patients.create')->with([
            'barangays' => $barangays,
            'cities' => $cities,
            'case_types' => $case_types, 
            'status_list' => $status_list
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $patient = new Patient;

        /** Form Validation **/
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'number' => 'required',
            'email' => "nullable|email|unique:patients,email,{$patient->id}",
            'case_type' => 'required'
        ]);
        
        /** Create Patient **/
        $patient->name = $request->input('name');
        $patient->barangay_id = $request->input('barangay');
        $patient->number = $request->input('number');
        $patient->email = $request->input('email');
        $patient->case_type = $request->input('case_type');
        $patient->coronavirus_status = $request->input('coronavirus_status');
        $patient->save();

        return redirect('/patients')->with('success', 'Patient Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /** Show patient information **/
         $patient = Patient::find($id);
         $barangay = Patient::find($id)->barangay;
         $barangay_id = Patient::find($id)->barangay_id;
         $city = Barangay::find($barangay_id)->city;
 
         return view('patients.show')->with(['patient' => $patient, 'barangay' => $barangay, 'city' => $city]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         /**  Edit patient **/
         $patient = Patient::find($id);
         $barangays = Barangay::all();
         $cities = City::all();
         $case_types = ["PUI", "PUM", "Positive", "Negative"];
         $status_list = ["Active", "Recovered", "Deceased"];
 
         return view('patients.edit')->with([
             'patient' => $patient, 
             'barangays' => $barangays, 
             'cities' => $cities, 
             'case_types' => $case_types,
             'status_list' => $status_list
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**  Form validation **/
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'number' => 'required',
            'email' => "nullable|email|unique:patients,email,{$id}",
            'case_type' => 'required'
        ]);

        /**  Update patient information **/
        $patient = Patient::find($id);
        $patient->name = $request->input('name');
        $patient->barangay_id = $request->input('barangay');
        $patient->number = $request->input('number');
        $patient->email = $request->input('email');
        $patient->case_type = $request->input('case_type');
        $patient->coronavirus_status = $request->input('covid_status');

        /* Send email notification user change case type of patient*/
        if($patient->isDirty('case_type')){
            Mail::to($patient->email)->send(new CaseTypeChangeMail($patient->case_type, $patient->name));
        }
        
        $patient->save();
        return redirect('/patients')->with('success', 'Patient Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**  Delete a patient **/
        $patient= Patient::find($id);
        $patient->delete();
        return redirect('/patients')->with('success', 'Patient Deleted!');
    }
}
