<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\City;
use App\Patient;
use App\Barangay;

class CoronaVirusReportsController extends Controller
{
    /** Authentication for pages **/
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**  Corona Virus Reports Views **/
    public function index(Request $request) {

        $city_id = $request->input('city');
        $barangay_id = $request->input('barangay');

        $barangays = Barangay::all();
        $cities = City::all();
        
        $city_request = City::find($city_id);
        $barangay_request = Barangay::find($barangay_id);

        /**  city result **/
        $city_results_positive = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $city_results_active = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.coronavirus_status', '=', 'Active']
                            ])->get();

        $city_results_recovered = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.coronavirus_status', '=', 'Recovered']
                            ])->get();

        $city_results_deceased = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.coronavirus_status', '=', 'Deceased']
                            ])->get();
        
        /**  city with barangay result **/                     
        $barangay_results_positive = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $barangay_results_active = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.coronavirus_status', '=', 'Active']
                            ])->get();

        $barangay_results_recovered = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.coronavirus_status', '=', 'Recovered']
                            ])->get();

        $barangay_results_deceased = DB::table('cities')
                            ->join('barangays', 'cities.id', '=', 'barangays.city_id')
                            ->join('patients', 'barangays.id', '=', 'patients.barangay_id')
                            ->select('patients.case_type', 'patients.coronavirus_status', 'barangays.city_id', 'barangays.id')
                            ->where([
                                ['barangays.city_id', '=', $city_id],
                                ['barangays.id', '=', $barangay_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.coronavirus_status', '=', 'Deceased']
                            ])->get();

        return view('coronavirus-report.index')->with([
            'city_request' => $city_request, 
            'barangay_request' => $barangay_request, 
            'cities' => $cities, 
            'barangays' => $barangays, 
            'city_id' => $city_id, 
            'barangay_id' => $barangay_id,
            'city_results_positive' => $city_results_positive,
            'city_results_active' => $city_results_active,
            'city_results_recovered' => $city_results_recovered,
            'city_results_deceased' => $city_results_deceased,
            'barangay_results_positive' => $barangay_results_positive,
            'barangay_results_active' => $barangay_results_active,
            'barangay_results_recovered' => $barangay_results_recovered,
            'barangay_results_deceased' => $barangay_results_deceased
        ]);
    }
}
