<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /** Authentication for pages **/
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        /**  Retrieve all cities **/
        $cities = City::orderBy('name', 'asc')->paginate(10);
        return view('cities.index')->with('cities', $cities);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**  Create new city **/
        return view('cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** Form Validation **/
        $request->validate([
            'name'=>'required'
        ]);

        /** Create City **/
        $city = new City;
        $city->name = $request->input('name');
        $city->save();
        return redirect('/cities')->with('success', 'City saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /** Show city information **/
        $city = City::find($id);
        return view('cities.show')->with('city', $city);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**  Edit city **/
        $city = City::find($id);
        return view('cities.edit')->with('city', $city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**  Form validation **/
        $request->validate([
            'name'=>'required'
        ]);

        /**  Update city information **/
        $city= City::find($id);
        $city->name =  $request->get('name');
        $city->save();
        return redirect('/cities')->with('success', 'City Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**  Delete a city **/
        $city= City::find($id);
        $city->delete();
        return redirect('/cities')->with('success', 'City Deleted!');
    }
}
