<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Patient;

class CheckStatusController extends Controller
{
     public function index(Request $request)
    {
        $patient = DB::table('patients');
        if( $request->input('search')){
            $patient = $patient->where('number', 'LIKE', "%" . $request->search . "%");
        }
        $patient= $patient->paginate(10);
      
        return view('patient-checkstatus.index')->with(['patient' => $patient]);
    }
}
