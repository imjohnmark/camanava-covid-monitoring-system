<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    // The attributes that are mass assignable
    protected $fillable = [
    'name'
    ];

    // Relationship many barangays to one city
    public function barangays(){
        return $this->HasMany(Barangay::class);
    }
}
