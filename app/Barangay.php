<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
    // The attributes that are mass assignable
    protected $fillable = [
        'name',
        'city_id'     
    ];

    // Relationship city to barangays
    public function city(){
        return $this->belongsTo(City::class);
    }

    // Relationship many patients to one barangay
    public function patients(){
        return $this->HasMany(Patient::class);
    }
}
