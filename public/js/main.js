let caseType = document.querySelector('#case-type');   // get the case type 
let coronavirusStatus = document.querySelector('#coronavirus-status'); // get the corona status 

if (caseType) { 
    let showCaseStatus = () => { 

        let caseStatusField = document.querySelector('.hidden');  // get hidden 
        
        // display case status if case type is positive
        if (caseType.value == "Positive") {
            caseStatusField.style.display = "block"; 
        } else {
            caseStatusField.style.display = "none";
            coronavirusStatus.value = '';
        }
    }
    
    caseType.addEventListener('change', showCaseStatus);   // funtion for case type changes
    window.load = showCaseStatus();  // function on load
}



// for awareness report and aorona report
let city = document.querySelector('#select-city'); // get the city 
let barangay = document.querySelector('#select-barangay'); // get the barangay 
let barangay_options = document.querySelectorAll('#select-barangay > option'); // get all barangay options
let generate_button = document.querySelector('#generate-report'); // get the generate button

if (city) { 

    let changeBarangayOptions = () => {

        // for each barangay options
        for (let barangay_option of barangay_options) {
            
            let barangay_city_id = barangay_option.getAttribute('city-data-id'); // get city-data-id value
            
            // display barangay options related to city
            if (city.value == barangay_city_id) {
                barangay_option.style.display = 'block';
            } else {
                barangay_option.style.display = 'none';
                barangay.firstElementChild.style.display = 'block'; 
            }
        }

    }

    let hideBarangayOptions = () => {
        
        // generate if city field is not empty 
        if (city.value.length != 0) {

            // if generate button exists
            if (generate_button) {
                generate_button.disabled = false; // enable generate button
            }
            
            barangay.disabled = false; // enable barangay field

        // else disable generate
        } else {
            // if generate button exists
            if (generate_button) {
                generate_button.disabled = true; // disable generate button
            }
            if(barangay.value.length == 0) {
                barangay.disabled = true; // disable barangay field
            }
        }
        changeBarangayOptions();
    }
    
    let showBarangay = () => {
    
        // if city is not empty
        if (city.value.length != 0) {

            // if generate button exists
            if (generate_button) {
                generate_button.disabled = false; // enable generate button
            }

            barangay.disabled = false; // enable barangay field
            barangay.value = ''; // set barangay field value to empty

        } else {
            // if generate button exists
            if (generate_button) {
                generate_button.disabled = true; // disable generate button
            }

            barangay.value = ''; // set barangay field value to empty
            barangay.disabled = true; // disable barangay field
        }
        changeBarangayOptions();
    }

    window.load = hideBarangayOptions(); // function on load
    city.addEventListener('change', showBarangay); // function for every city changes

}



let hideList = () => { 

    let CaseTypeParent = document.querySelector('.patientCaseType'); 
    
    // if case type is more than 1 (to hide list)
    if (CaseTypeParent.childElementCount > 1) {
        CaseTypeParent.style.display="none"; 
    } else {
        CaseTypeParent.style.display="block"; 
    }
}

window.load = hideList(); 


